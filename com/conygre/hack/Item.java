package com.conygre.hack;

public abstract class Item {
    protected Boolean allowBorrow;
    protected String name;
    protected int id;

    public Boolean getAllowBorrow() {
        return allowBorrow;
    }

    public void setAllowBorrow(Boolean allowBorrow) {
        this.allowBorrow = allowBorrow;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}