package com.conygre.hack;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        
        // users
        User edward = new User("Edward");
        User jonah = new User("Jonah");
        User abdullah = new User("Abdullah");

        // Items
        ArrayList <Item> items = new ArrayList<>();
        items.add(new Book(1, "batman book"));
        items.add(new Book(2, "spiderman book"));
        items.add(new CD(3, "batman movie"));
        items.add(new CD(4, "spiderman movie", false));

        // adding items to Library
        Library library = new Library(items);

        // addding library to library Manager
        LibraryManager libraryManager = new LibraryManager(library);

        System.out.println("\n--------------------\n");

        // normal case 
        libraryManager.itemborrowed(1, edward);
        libraryManager.itemReturned(1, edward);

        System.out.println("\n--------------------\n");

        // double borrow
        libraryManager.itemborrowed(1, edward);
        libraryManager.itemborrowed(1, edward);
        libraryManager.itemReturned(1, edward);

        System.out.println("\n--------------------\n");

        // double borrow different person
        libraryManager.itemborrowed(1, edward);
        libraryManager.itemborrowed(1, abdullah);
        libraryManager.itemReturned(1, edward);

        System.out.println("\n--------------------\n");

        // wrong person return 
        libraryManager.itemborrowed(1, edward);
        libraryManager.itemReturned(1, abdullah);
        libraryManager.itemReturned(1, edward);

        System.out.println("\n--------------------\n");

        // unborrow return 
        libraryManager.itemReturned(1, abdullah);
 
    }
}