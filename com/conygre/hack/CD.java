package com.conygre.hack;

public class CD extends Item{

    public CD(int id, String name){
        this(id, name, true);
    }

    public CD(int id, String name, Boolean allowBorrow){
        this.id = id;
        this.name = name;
        this.allowBorrow = allowBorrow;
    }
}