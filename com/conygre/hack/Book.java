package com.conygre.hack;

public class Book extends Item{

    public Book(int id, String name){
        this(id, name, true);
    }

    public Book(int id, String name, Boolean allowBorrow){
        this.id = id;
        this.name = name;
        this.allowBorrow = allowBorrow;
    }
} 