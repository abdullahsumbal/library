package com.conygre.hack;
import java.util.HashMap;
import java.util.ArrayList;

public class Library {

    private HashMap <Integer, Item> items = new HashMap<>();

    public Library(ArrayList<Item> items){

        for (Item item: items){
            this.items.put(item.getId(), item);
        }
    }

    public void addtoLibrary(int id, Item item){
        items.put(id, item);
    }

    public Item getItem(int id){
        return items.get(id);
    }

    public Boolean hasItem(int id){
        return items.containsKey(id);
    }

    public HashMap<Integer, Item> getItems(){
         
        return new HashMap<Integer, Item>(items);
    }
}