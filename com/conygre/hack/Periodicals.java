package com.conygre.hack;

public class Periodicals extends Item  {

    public Periodicals(int id, String name){
        this(id, name, false);
    }

    public Periodicals(int id, String name, Boolean allowBorrow){
        this.id = id;
        this.name = name;
        this.allowBorrow = allowBorrow;
    }
}