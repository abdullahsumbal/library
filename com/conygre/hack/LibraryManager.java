package com.conygre.hack;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LibraryManager {
    private Library library;

    public LibraryManager(Library library){
        this.library = library;
        this.avaiableItems = library.getItems();
        borrowedItems = new HashMap<>();
    }

    private HashMap <Integer, Item> avaiableItems;
    private HashMap <Integer, User> borrowedItems;

    public void itemborrowed(int id, User user){
        Item borrowedItem = library.getItem(id);
    
        if (hasItem(id) && borrowedItem.getAllowBorrow()){
            avaiableItems.remove(id);
            borrowedItems.put(id, user);
            System.out.println("item: " + borrowedItem.getName() + " is borrowed by user:" +  user.getName());
        }
        else{
            System.out.println("item: "+ borrowedItem.getName() + " is not allowed to be borrowed");
        }

    }

    public void itemReturned(int id, User user){
        if (user != borrowedItems.get(id)){
            System.out.println("user: " + user.getName() + " is not the correct user to return this book");
            return;
        }
        Item returnedItem = library.getItem(id);
        borrowedItems.remove(id);
        avaiableItems.put(id, returnedItem);
        System.out.println("item: "+ returnedItem.getName() + " is returned by user: "+ user.getName());
    }

    public Boolean hasItem(int id){
        return avaiableItems.containsKey(id);
    }
    
}